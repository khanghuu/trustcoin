<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $primaryKey = 'User_ID';

    protected $fillable = ['User_ID', 'User_Name', 'User_Email', 'User_Agency_Level'];

    public $timestamps = false;


}
