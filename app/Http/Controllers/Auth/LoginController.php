<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Login;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use function Composer\Autoload\includeFile;

class LoginController extends Controller
{

    public function getLoginRegister()
    {
        return view('Auth.Login-Register');
    }

    public function postLogin(Login $request)
    {
        $loginUser = User::where('User_Email', $request->email)->first();

        if (!Hash::check($request->password, $loginUser->User_Password)) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Password incorrect']);
        }

        Session::put('user', $loginUser);

        return redirect()->route('Dashboard')->with(['flash_level' => 'success', 'flash_message' => 'Login successfully']);

    }

    public function getLogout()
    {
        Session::forget('user');
        $userTemp = session('user_temp');
        if ($userTemp) {
            Session::put('user', session('user', $userTemp));
            Session::forget('user_temp');
            return redirect()->route('Dashboard')->with(['flash_level' => 'success', 'flash_message' => 'Login successfully']);
        }
        return redirect()->route('getLoginRegister')->with(['flash_level' => 'success', 'flash_message' => 'Logout successfully']);
    }
}
