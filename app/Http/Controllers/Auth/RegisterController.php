<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Register;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    public function postRegister(Register $request)
    {
        $sponserInfo = User::where('User_ID', $request->sponser)->first();

        $userID = $this->RandomIDUser();

        $userTree = $sponserInfo->User_Tree . "," . $userID;

        $userData = [
            'User_ID' => $userID,
            'User_Email' => $request->email,
            'User_Parent' => $request->sponser,
            'User_Tree' => $userTree,
            'User_Password' => bcrypt($request->password),
            'User_RegisteredDatetime' => date('Y-m-d H:i:s'),
            'User_Level' => 0,
            'User_Status' => 0,
        ];
        $inserID = DB::table('users')->insertGetID($userData);

        if (!$inserID) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'There is an error, please contact admin']);
        }

//        //Tạo token cho mail
//        $dataToken = array('user_id' => $userID, 'time' => time());
//
//        //$token = base64_encode(xxtea_encrypt(json_encode($dataToken),$keyHash));
//        $token = encrypt(json_encode($dataToken));
//
//        //dữ liệu gửi sang mailtemplate
//        $data = array('User_Email' => $request->email, 'token' => urlencode($token));
//
//        // gửi mail thông báo
//        Mail::send('Mail.Active', $data, function ($msg) use ($userData) {
//            $msg->from('do-not-reply@skynet4fx.com', 'SkyNet4FX');
//            $msg->to($userData['User_Email'])->subject('Login SkyNet4FX');
//        });

        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Registration successful, please check your email to confirm!']);
    }

    public function RandomIDUser()
    {
        $id = rand(100000, 999999);
        //TẠO RA ID RANĐOM
        $user = User::where('User_ID', $id)->first();
        //KIỂM TRA ID RANDOM ĐÃ CÓ TRONG USER CHƯA
        if (!$user) {
            return $id;
        }
        else {
            return $this->RandonIDUser();
        }
    }

    public function getActive(Request $req)
    {
        // cập nhật lại mail đã active
        include(app_path() . '/functions/xxtea.php');

        if ($req->token) {

            $token = ($req->token);
            $keyHash = $this->keyHash;

            $data = json_decode(decrypt(urldecode($token)));

            if (strtotime('+10 minutes', $data->time) < time()) {
                return redirect()->route('getLoginRegister')->with(['flash_level' => 'error', 'flash_message' => 'This mail expired! Please Login again']);
            }
            $user = User::where('User_ID', $data->user_id)->where('User_Status', 1)->first();

            if ($user) {
                if ($user->User_EmailActive == 0) {
                    $user->User_EmailActive = 1;
                    $user->save();
                }
                Session::put('user', $user);
                if ($req->redirect) {
                    return redirect()->to($req->redirect);
                }
                return redirect()->route('dashboard')->with(['flash_level' => 'success', 'flash_message' => 'Login Success!']);
            }
        }

        return redirect()->route('getLoginRegister')->with(['flash_level' => 'error', 'flash_message' => 'Please Login Again!']);
    }


}
