<?php

namespace App\Http\Controllers\Cron;

use App\Models\Money;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Investment;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CronController extends Controller
{

    public function getDeposit()
    {

        $moneyRate = $this->getHttp("https://trustexc.com/api/ticker")[0]->price_usd;

        $usersAddress = Wallet::select('Address_User', 'Address_Address')->get();

        foreach ($usersAddress as $userAddress) {
            $transactions = $this->getHttp("https://trustexc.com/api/get_transactions_by_address?address=$userAddress->Address_Address");
           foreach ($transactions->data as $transaction)
           {
               if ($transaction->category == 'receive'){
                   $checkDeposit = Money::where('Money_TXID', $transaction->txid)->first();

                   if (!$checkDeposit) {
                       $depositData = [
                           'Money_User' => $userAddress->Address_User,
                           'Money_USDT' => $transaction->amount,
                           'Money_Time' => $transaction->time,
                           'Money_Comment' => 'Deposit Trustcoin',
                           'Money_MoneyAction' => 1,
                           'Money_MoneyStatus' => 1,
                           'Money_TXID' => $transaction->txid,
                           'Money_Address' => $userAddress->Address_Address,
                           'Money_Currency' => 1,
                           'Money_CurrentAmount' => $transaction->amount,
                           'Money_Rate' => floatval($moneyRate),
                           'Money_Confirm' => 1
                       ];
                       Money::create($depositData);
                   }
               }


           }
        }

    }

    protected function getHttp($url)
    {
        $client = new Client();
        $response = $client->get($url);
        return json_decode($response->getBody());
    }

    public function payInterest()
    {
        $investmentStatictis = Investment::selectRaw("investment_User, sum(investment_Amount) as 'amount'")
            ->where('investment_Status', 1)
            ->groupBy('investment_User')
            ->get();
        foreach ($investmentStatictis as $investmentItem) {
            $interestPaid = Money::where('Money_User', $investmentItem->investment_User)
                ->where('Money_MoneyAction', 4)
                ->where('Money_Confirm', 1)
                ->sum('Money_USDT');
            $totalPay = 1.5 * $investmentItem->amount;
            $interestDaily = 0.004 * $totalPay;
            $moneyRate = $this->getHttp("https://trustexc.com/api/ticker")[0]->price_usd;
            if (($interestDaily + $interestPaid) <= $totalPay){
                $interestData = [
                    'Money_User' => $investmentItem->investment_User,
                    'Money_USDT' => $interestDaily,
                    'Money_Time' => time(),
                    'Money_Comment' => 'Interest Daily',
                    'Money_MoneyAction' => 4,
                    'Money_MoneyStatus' => 1,
                    'Money_Currency' => 1,
                    'Money_CurrentAmount' => $interestDaily,
                    'Money_Rate' => floatval($moneyRate),
                    'Money_Confirm' => 1
                ];
                Money::create($interestData);
                $this->payInterestToAgencyLevel($investmentItem->investment_User, $interestDaily);
            }
            else {
                if ($interestPaid < $totalPay)
                {
                    $interestDaily = $totalPay - $interestPaid;
                    $interestData = [
                        'Money_User' => $investmentItem->investment_User,
                        'Money_USDT' => $interestDaily,
                        'Money_Time' => time(),
                        'Money_Comment' => 'Interest Daily',
                        'Money_MoneyAction' => 4,
                        'Money_MoneyStatus' => 1,
                        'Money_Currency' => 1,
                        'Money_CurrentAmount' => $interestDaily,
                        'Money_Rate' => floatval($moneyRate),
                        'Money_Confirm' => 1
                    ];
                    Money::create($interestData);
                    $this->payInterestToAgencyLevel($investmentItem->investment_User, $interestDaily);
                }
            }
        }
    }

    public function payInterestToAgencyLevel($userID, $interestDaily)
    {
        $percenInteret = [0, 0.05, 0.1, 0.15, 0.2];
        $moneyRate = $this->getHttp("https://trustexc.com/api/ticker")[0]->price_usd;
        $userTree = User::where('User_ID', $userID)->value('User_Tree');
        $usersArray = explode(',', $userTree);
        $usersArray = array_reverse($usersArray);
        unset($usersArray[0]);
        foreach ($usersArray as $user) {
            $userAgencyLevel = User::where('User_ID', $user)->value('User_Agency_Level');
            if ($userAgencyLevel) {
                $interestData = [
                    'Money_User' => $user,
                    'Money_USDT' => $interestDaily*$percenInteret[$userAgencyLevel],
                    'Money_Time' => time(),
                    'Money_Comment' => 'Interest System',
                    'Money_MoneyAction' => 6,
                    'Money_MoneyStatus' => 1,
                    'Money_Currency' => 1,
                    'Money_CurrentAmount' => $interestDaily*$percenInteret[$userAgencyLevel],
                    'Money_Rate' => floatval($moneyRate),
                    'Money_Confirm' => 1
                ];
                Money::create($interestData);
            }
        }
    }
}

