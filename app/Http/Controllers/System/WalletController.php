<?php

namespace App\Http\Controllers\System;

use App\Models\Money;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use vendor\project\StatusTest;
use GuzzleHttp\Client;

class WalletController extends Controller
{
    public function index()
    {
        $user =  session('user');
        $depositAmount = Money::where('Money_User', $user->User_ID)->where('Money_MoneyAction' ,1)->select('Money_USDT', 'Money_Time', 'Money_Rate')->get();
        $coinAmount = Money::where('Money_User', $user->User_ID)->where('Money_Confirm', 1)->sum('Money_USDT');
        return view('System.Wallet.My-Wallet', compact('depositAmount', 'coinAmount'));
    }

    public function getWalletAddress() {
        $userID = session('user')->User_ID;

        $walletAddress = Wallet::where('Address_User', $userID)->value('Address_Address');

        if (!$walletAddress) {
            $client = new Client();

            $response = $client->get('https://trustexc.com/api/generate_address');

            $responseData = json_decode($response->getBody());

            $walletAddress = $responseData->data->address;

            $addressData = [
                'Address_Currency' => 1,
                'Address_Address' => $walletAddress,
                'Address_User' => $userID,
                'Address_CreateAt' => date('Y-m-d H:i:s'),
                'Address_UpdateAt' => date('Y-m-d H:i:s'),
                'Address_IsUse' => 1,
                'Address_Comment' => 'Create new address'
            ];
            Wallet::create($addressData);
        }
        return response()->json(['address' => $walletAddress, 'status' => 'success'], 200);
    }

    protected function postWithdraw(Request $request)
    {
        if (!$request->amount_coin || !is_numeric($request->amount_coin)) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Amount coin invalid']);
        }
        if (!$request->wallet_address) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Amount usd incorrect']);
        }

        $user =  session('user');
        $coinAmount = Money::where('Money_User', $user->User_ID)->sum('Money_USDT');

        if ($request->amount_coin > $coinAmount) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Not enough coin']);
        }
        $moneyRate = $this->getHttp("https://trustexc.com/api/ticker")[0]->price_usd;
        $depositData = [
            'Money_User' => $user->User_ID,
            'Money_USDT' => -$request->amount_coin,
            'Money_USDTFee' => 0.3,
            'Money_Time' => time(),
            'Money_Comment' => 'Withdraw Trustcoin',
            'Money_MoneyAction' => 2,
            'Money_MoneyStatus' => 1,
            'Money_Address' => $request->wallet_address,
            'Money_Currency' => 1,
            'Money_CurrentAmount' => -$request->amount_coin,
            'Money_Rate' => floatval($moneyRate),
            'Money_Confirm' => 0
        ];
        $insertStatus = Money::create($depositData);
        if ($insertStatus) {
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Withdrawal order successful']);
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'There is an error, please contact admin']);
    }

    protected function getHttp($url)
    {
        $client = new Client();
        $response = $client->get($url);
        return json_decode($response->getBody());
    }

    public function getHistoryWallet()
    {
        $user = session('user');
        $walletHistory = Money::whereRaw("Money_User = $user->User_ID AND Money_MoneyAction = 1 AND Money_Confirm = 1 AND Money_MoneyStatus = 1")
            ->select('Money_ID', 'Money_USDT','Money_USDTFee', 'moneyaction.MoneyAction_Name', 'Money_Rate','Money_Time', 'Money_Comment')
            ->join('moneyaction', 'Money_MoneyAction', 'moneyaction.MoneyAction_ID')
            ->get()->toArray();
        $withdrawHistory = Money::whereRaw("Money_User = $user->User_ID AND Money_MoneyAction = 2 AND Money_Confirm = 1 AND Money_MoneyStatus = 1")
            ->select('Money_ID', 'Money_USDT', 'Money_USDTFee', 'moneyaction.MoneyAction_Name', 'Money_Rate','Money_Time', 'Money_Comment')
            ->join('moneyaction', 'Money_MoneyAction', 'moneyaction.MoneyAction_ID')
            ->get()->toArray();
        $walletHistory = collect($walletHistory);
        $walletHistory = $walletHistory->merge($withdrawHistory)->all();
        return view('System.History.Wallet-History', compact('walletHistory'));
    }


}
