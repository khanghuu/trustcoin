<?php

namespace App\Http\Controllers\System;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function getAdminUsersList()
    {
        $usersList = User::join('user_agency_level', 'User_Agency_Level', '=', 'user_agency_level.user_agency_level_ID')
            ->join('user_level', 'User_Level', '=', 'user_level.User_Level_ID')
            ->select('User_ID', 'User_Email', 'User_Status','user_level.User_Level_Name', 'User_RegisteredDatetime', 'User_Parent', 'user_agency_level.user_agency_level_Name')
            ->get();
        return view('System.Admin.User', compact('usersList'));
    }

    public function autoLogin($id)
    {
        $user = User::where('User_ID', $id)->first();
        $userTemp = session('user');
        Session::forget('user');
        Session::put('user_temp', $userTemp);
        Session::put('user', $user);
        return redirect()->route('Dashboard');
    }
    public function getAdminInvestmentList()
    {
        return view('System.Admin.Investment');
    }
    public function getAdminWalletList()
    {
        return view('System.Admin.Wallet');
    }

    public function getAdminMemberList ()
    {
        return view('System.Admin.Member');
    }
}
