<?php

namespace App\Http\Controllers\System;

use App\Models\Money;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommissionController extends Controller
{
        public function getHistoryCommisson()
        {
            $user = session('user');
            $commissionList = Money::where('Money_User', $user->User_ID)
                ->where('Money_MoneyAction', 5)
                ->where('Money_Confirm', 1)
                ->select('Money_ID', 'Money_USDT', 'Money_Rate','Money_Time', 'Money_Comment')
                ->get();
            return view('System.History.Commission-History', compact('commissionList'));
        }
}
