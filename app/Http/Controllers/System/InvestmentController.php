<?php

namespace App\Http\Controllers\System;

use App\Models\Investment;
use App\Models\Money;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class InvestmentController extends Controller
{
    public function index()
    {
        $user = session('user');
        $investmentList = Investment::where('investment_User', $user->User_ID)
            ->select('investment_Amount', 'investment_Rate', 'investment_Time', 'investment_Status')
            ->where('investment_Status', 1)
            ->get();
        $coinAmount = Money::where('Money_User', $user->User_ID)->where('Money_Confirm', 1)->sum('Money_USDT');
        $investmentAmount = Investment::where('investment_User', $user->User_ID)
            ->where('investment_Status', 1)
            ->sum('investment_Amount');
        $totalProfit = $investmentAmount * 1.5;
        $dailyInterest = $investmentAmount * 1.5 * 0.004;
        return view('System.Investment.Index', compact('investmentList', 'coinAmount', 'investmentAmount', 'totalProfit', 'dailyInterest'));
    }

    public function getHistoryInvestment()
    {
        $user = session('user');
        $investmentHistory = Investment::where('investment_User', $user->User_ID)
            ->select('investment_ID', 'investment_Amount', 'investment_Rate', 'investment_Time', 'investment_Status')
            ->get();
        return view('System.History.Investment-History', compact('investmentHistory'));
    }
    public function postInvestment(Request $request)
    {
        if (!$request->investment_amount || !is_numeric($request->investment_amount)) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Investment amount invalid']);
        }

        $user =  session('user');
        $coinAmount = Money::where('Money_User', $user->User_ID)->sum('Money_USDT');

        if ($request->investment_amount > $coinAmount) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Not enough coin']);
        }
        if ($request->investment_amount < 1000) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Minimum investment of 1,000']);
        }
        $moneyRate = $this->getHttp("https://trustexc.com/api/ticker")[0]->price_usd;

        $investmentData = [
            'investment_User' =>  $user->User_ID,
            'investment_Amount' => $request->investment_amount,
            'investment_Currency' => 1,
            'investment_Rate' => floatval($moneyRate),
            'investment_Time' => time(),
            'investment_Status' => 1
        ];
        $insertInvestmentStatus = Investment::create($investmentData);
        $moneyData = [
            'Money_User' => $user->User_ID,
            'Money_USDT' => -$request->investment_amount,
            'Money_Time' => time(),
            'Money_Comment' => 'Inverstment Trustcoint',
            'Money_MoneyAction' => 3,
            'Money_MoneyStatus' => 1,
            'Money_Address' => $request->wallet_address,
            'Money_Currency' => 1,
            'Money_CurrentAmount' => -$request->investment_amount,
            'Money_Rate' => floatval($moneyRate),
            'Money_Confirm' => 1
        ];
        $insertMoneyStatus = Money::create($moneyData);

        $this->payDirectCommission($user->User_ID, $request->investment_amount);
        $this->checkToLevel($user->User_ID);
        if ($insertInvestmentStatus && $insertMoneyStatus) {
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Investment successful']);
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'There is an error, please contact admin']);
    }

    protected function getHttp($url)
    {
        $client = new Client();
        $response = $client->get($url);
        return json_decode($response->getBody());
    }

    public function payDirectCommission($userID, $investment_Amount, $f = 1)
    {
        $userParent = User::where('User_ID', $userID)->value('User_Parent');
        if ($userParent) {
            $moneyRate = $this->getHttp("https://trustexc.com/api/ticker")[0]->price_usd;
            if ($f == 1) {
                $moneyData = [
                    'Money_User' => $userParent,
                    'Money_USDT' => 0.05 * $investment_Amount,
                    'Money_Time' => time(),
                    'Money_Comment' => 'Direct Commission',
                    'Money_MoneyAction' => 5,
                    'Money_MoneyStatus' => 1,
                    'Money_Currency' => 1,
                    'Money_CurrentAmount' => 0.05 * $investment_Amount,
                    'Money_Rate' => floatval($moneyRate),
                    'Money_Confirm' => 1
                ];
            }
            if ($f == 2) {
                $moneyData = [
                    'Money_User' => $userParent,
                    'Money_USDT' => 0.04 * $investment_Amount,
                    'Money_Time' => time(),
                    'Money_Comment' => 'Direct Commission',
                    'Money_MoneyAction' => 5,
                    'Money_MoneyStatus' => 1,
                    'Money_Currency' => 1,
                    'Money_CurrentAmount' => 0.04 * $investment_Amount,
                    'Money_Rate' => floatval($moneyRate),
                    'Money_Confirm' => 1
                ];
            }
            if ($f == 3) {
                $moneyData = [
                    'Money_User' => $userParent,
                    'Money_USDT' => 0.03 * $investment_Amount,
                    'Money_Time' => time(),
                    'Money_Comment' => 'Direct Commission',
                    'Money_MoneyAction' => 5,
                    'Money_MoneyStatus' => 1,
                    'Money_Currency' => 1,
                    'Money_CurrentAmount' => 0.03 * $investment_Amount,
                    'Money_Rate' => floatval($moneyRate),
                    'Money_Confirm' => 1
                ];
            }
            if ($f == 4) {
                $moneyData = [
                    'Money_User' => $userParent,
                    'Money_USDT' => 0.02 * $investment_Amount,
                    'Money_Time' => time(),
                    'Money_Comment' => 'Direct Commission',
                    'Money_MoneyAction' => 5,
                    'Money_MoneyStatus' => 1,
                    'Money_Currency' => 1,
                    'Money_CurrentAmount' => 0.02 * $investment_Amount,
                    'Money_Rate' => floatval($moneyRate),
                    'Money_Confirm' => 1
                ];
            }
            if ($f == 5) {
                $moneyData = [
                    'Money_User' => $userParent,
                    'Money_USDT' => 0.01 * $investment_Amount,
                    'Money_Time' => time(),
                    'Money_Comment' => 'Direct Commission',
                    'Money_MoneyAction' => 5,
                    'Money_MoneyStatus' => 1,
                    'Money_Currency' => 1,
                    'Money_CurrentAmount' => 0.01 * $investment_Amount,
                    'Money_Rate' => floatval($moneyRate),
                    'Money_Confirm' => 1
                ];
            }
            Money::create($moneyData);
            if ($f <= 5){
                $this->payDirectCommission($userParent, $investment_Amount, ++$f);
            }
        }
    }

    public function checkToLevel($userID)
    {
        $userTree = User::where('User_ID', $userID)->value('User_Tree');
        $usersArray = explode(",", $userTree);
        $usersArray = array_reverse($usersArray);
        unset($usersArray[0]);
        foreach ($usersArray as $user) {
            $this->checkLevel($user);
        }
    }

    public function checkLevel($userID) {

        $treeAmountInvest  = $this->getTreeInvest($userID);
        $userAgencyLevel = 0;
        if ($treeAmountInvest >= 200000) {
            $userAgencyLevel = 1;
        }

        $childrenLevel2 = User::where('User_Parent', $userID)->where('User_Agency_Level', '>=', 1)->get()->count();
        if ($childrenLevel2 >= 2) {
            $userAgencyLevel = 2;
        }

        $childrenLevel3 = User::where('User_Parent', $userID)->where('User_Agency_Level', '>=', 2)->get()->count();
        if ($childrenLevel3 >= 2) {
            $userAgencyLevel = 3;
        }

        $childrenLevel4 = User::where('User_Parent', $userID)->where('User_Agency_Level', '>=', 3)->get()->count();
        if ($childrenLevel4 >= 2) {
            $userAgencyLevel = 4;
        }
        User::where('User_ID', $userID)->update(['User_Agency_Level'=> $userAgencyLevel]);

    }

    public function getTreeInvest($userID) {
        $userTree = User::where('User_ID', $userID)->value('User_Tree');
        $amount = Investment::join('users', 'Investment_User', 'users.User_ID')
            ->where('users.User_Tree', 'like', "$userTree,%")
            ->where('investment_Status', 1)
            ->selectRaw('investment_Amount*investment_Rate as a')->get()->sum('a');
        return $amount;

    }

}

