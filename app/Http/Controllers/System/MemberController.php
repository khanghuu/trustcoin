<?php

namespace App\Http\Controllers\System;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    public function getMembersList()
    {
        $userID = session('user')->User_ID;
        $membersList = $this->getMembers($userID);
        return view('System.Members.Members-List', compact('membersList'));
    }

    public function getMembersTree(Request $request)
    {
        if (!$request->userID) {
            $userID = session('user')->User_ID;
        }
        $usersTreeList = [];
        $user = User::Where('User_Tree', 'like', "$userID")
            ->orWhere('User_Tree', 'like', "%$userID")
            ->select('User_ID as id', 'User_Parent as pid','User_Email as email')
            ->first()->toArray();
        $user['img'] = "dist/img/user.png";
        array_push($usersTreeList, $user);
        $this->getUsersTreeList($userID, $usersTreeList);
        return view('System.Members.Members-Tree', compact('usersTreeList'));
    }

    public function getUsersTreeList($userID, &$usersTreeList)
    {
        $children = User::Where('User_Parent', $userID)
            ->select('User_ID as id', 'User_Parent as pid', 'User_Email as email')
            ->get();
        if ($children) {
            foreach ($children as $child) {
                $child->img = "dist/img/user.png";
                array_push($usersTreeList, $child->toArray());
                $this->getUsersTreeList($child->id, $usersTreeList);
            }
        }
    }

    public function getMembers($userID, $level = 0) {
        ++$level;
       $membersList = User::where('User_Parent', $userID)
           ->select('User_ID', 'User_Email', 'User_RegisteredDatetime', 'User_Parent')
           ->get();
       if ($membersList) {
           foreach ($membersList as $user) {
               $user->User_F = $level;
              $user->children = $this->getMembers($user->User_ID, $level);
           }
       }

       return $membersList->toArray();
    }



}
