<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Winkle I Fast build Admin dashboard for any platform</title>
		<meta name="description" content="Winkle is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Winkle Admin, Winkleadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
        <base href="{{asset('')}}">
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

		<!-- vector map CSS -->



		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body style="background-image: url('../img/LOGO/bg1.jpg');width: 100%;background-position: center;background-repeat: no-repeat;background-size: cover;margin: 0;padding: 0;">
		<!--Preloader-->
			<div class="bg-color" style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;width: 100;background: hsla(214, 94%, 21%, 0.65);"></div>
			<div class="form-structor">
				<div class="signup">
					<h2 class="form-title" id="signup">RESET PASSWORD</h2>
                    <form action="{{route('postForgotPassword')}}" method="post">
                        @csrf
                        <div class="form-holder style-input style-input1">
                            <input type="email" name="email" class="input" placeholder="Email" style="color: #fff" />
                        </div>
                        <button class="submit-btn">Submit</button>
                    </form>
				</div>
			</div>
		<!--/Preloader-->

			<!-- /Main Content -->

		<!-- /#wrapper -->

		<!-- JavaScript -->

		<!-- jQuery -->
		<!-- <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

		Bootstrap Core JavaScript
		<script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="../vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script> -->

		<!-- Slimscroll JavaScript -->
		<!-- <script src="dist/js/jquery.slimscroll.js"></script> -->

		<!-- Init JavaScript -->
		<!-- <script src="dist/js/init.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
		<script type="text/javascript">

            @if(Session::get('flash_level') == 'success')
            toastr.success('{{ Session::get('flash_message') }}', 'Success!', {timeOut: 3500})
            @elseif(Session::get('flash_level') == 'error')
            toastr.error('{{ Session::get('flash_message') }}', 'Error!', {timeOut: 3500})
            @endif

            @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
            toastr.error('{{$error}}', 'Error!', {timeOut: 3500})
            @endforeach
            @endif
		</script>
	</body>
</html>
