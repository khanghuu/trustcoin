<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>TrustCoin-Login</title>
    <meta name="description" content="Winkle is a Dashboard & Admin Site Responsive Template by hencework." />
    <meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Winkle Admin, Winkleadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
    <meta name="author" content="hencework"/>
    <base href="{{asset('')}}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <!-- vector map CSS -->



    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body style="background-image: url('img/LOGO/bg1.jpg');width: 100%;background-position: center;background-repeat: no-repeat;background-size: cover;margin: 0;padding: 0;">
<!--Preloader-->
<div class="bg-color" style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;width: 100;background: hsla(214, 94%, 21%, 0.65);"></div>
<div class="form-structor">
    <form method="post" action="{{route('postLogin')}}">
        @csrf
        <div class="signup">
            <h2 class="form-title" id="signup">Sign In</h2>
            <div class="form-holder style-input style-input1">
                <input type="email" name="email" class="input" placeholder="Email" style="color: #fff" />
                <input type="password" name="password" class="input" placeholder="Password" style="color: #fff" />
            </div>
            <button class="submit-btn">Sign in</button>
            <span class="text-right" style="float: right;"><a href="{{route('getForgotPassword')}}" style="color: #fff;text-decoration: none;"></ABBR>Forgot password</a></span>
        </div>
    </form>
    <form method="post" action="{{route('postRegister')}}">
        @csrf
        <div class="login slide-up">
            <div class="center">
                <h2 class="form-title" id="login"><span>or</span>Sign up</h2>
                <div class="form-holder style-input style-input2">
                    <input type="email" name="email" class="input" placeholder="Email" required/>
                    <input type="password" name="password" class="input" placeholder="Password" required/>
                    <input type="password" name="password_comfirm" class="input" placeholder="Re-password" required/>
                    <input type="text" name="sponser" class="input" placeholder="Sponser ID" required/>
                </div>
                <button class="submit-btn">Sign up</button>
            </div>
        </div>
    </form>

</div>

<!--/Preloader-->

<!-- /Main Content -->

<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<!-- <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

Bootstrap Core JavaScript
<script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script> -->

<!-- Slimscroll JavaScript -->
<!-- <script src="dist/js/jquery.slimscroll.js"></script> -->

<!-- Init JavaScript -->
<!-- <script src="dist/js/init.js"></script> -->
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
    console.clear();

    const loginBtn = document.getElementById('login');
    const signupBtn = document.getElementById('signup');

    loginBtn.addEventListener('click', (e) => {
        let parent = e.target.parentNode.parentNode;
        Array.from(e.target.parentNode.parentNode.classList).find((element) => {
            if(element !== "slide-up") {
                parent.classList.add('slide-up')
            }else{
                signupBtn.parentNode.classList.add('slide-up')
                parent.classList.remove('slide-up')
            }
        });
    });

    signupBtn.addEventListener('click', (e) => {
        let parent = e.target.parentNode;
        Array.from(e.target.parentNode.classList).find((element) => {
            if(element !== "slide-up") {
                parent.classList.add('slide-up')
            }else{
                loginBtn.parentNode.parentNode.classList.add('slide-up')
                parent.classList.remove('slide-up')
            }
        });
    });
    @if(Session::get('flash_level') == 'success')
        toastr.success('{{ Session::get('flash_message') }}', 'Success!', {timeOut: 3500})
    @elseif(Session::get('flash_level') == 'error')
        toastr.error('{{ Session::get('flash_message') }}', 'Error!', {timeOut: 3500})
    @endif

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            toastr.error('{{$error}}', 'Error!', {timeOut: 3500})
        @endforeach
    @endif
</script>
</body>
</html>
