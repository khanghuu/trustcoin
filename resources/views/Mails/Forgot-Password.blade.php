<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Mail KFC</title>
	<style type="text/css">
		.col-8{
			margin: auto;
			text-align: center;
		}
		table{
			background-size: contain;
			background-repeat: no-repeat;
			height: 897px;
			width: 	768px;
			border: 2px solid #158ece;
			margin: auto;
		}
		h1,h2{
			color: white;
		}
		.announce{
			color: white;
			height: 2%;
		}
		.welcome{
			height: 65%;
			padding-top: 80%;
		}
		.tx1{
			padding-bottom: 5px;
			color: #fff;
		}
		.tx{
			padding-bottom: 20px;
			color: #fff;
		}
		button{
			padding: 5px 10px;
		    border: 2px solid white;
		    background: linear-gradient(45deg, #085b7c, transparent, #085b7c);
		    width: 120px;
		    font-weight: bold;
		    transition: 0.5s;
		}
		a{
			color: white;
		}
		button:hover{
			background: white;
			color: #099dd0;
		}
		button:hover a{
			text-decoration: none;
		}
	</style>
</head>
<body>

	<div class="container-fluid">
		<div class="row">
			<div class="col-8">
				<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="15" background="img/mail/img.png">
					<tr>
						<td class="announce"></td>
					</tr>
					<tr>
						<td class="announce">
						</td>
					</tr>
					<tr>
						<td class="welcome">
							<div class="tx1">YOU HAVE SUCCESSFULLY FORGOT PASSWORD</div>
							<div class="tx">NEW PASSWORD: <span style="color: #ffeb3b">{{$password}}</span></div>
							<!-- <div><button><a href="">Click here</a></button></div> -->
						</td>
					</tr>

					<tr>
						<td><!-- ADD MORE INFO HERE --></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

</body>
</html>
