<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>@yield('title')</title>
    <meta name="description" content="Winkle is a Dashboard & Admin Site Responsive Template by hencework." />
    <meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Winkle Admin, Winkleadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
    <meta name="author" content="hencework"/>
    <base href="{{asset('')}}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="../img/LOGO/logo2-01.png">
    <link rel="icon" href="../img/LOGO/logo2-01.png" type="image/x-icon">

    <!-- Data table CSS -->
    <link href="../vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

    <!-- Toast CSS -->
    <link href="../vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

    <!-- Morris Charts CSS -->
    <link href="../vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- select2 CSS -->
    <link href="../vendors/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>

    <!-- switchery CSS -->
    <link href="../vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>

    <!-- bootstrap-select CSS -->
    <link href="../vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
    <!-- vector map CSS -->
    <link href="../vendors/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" type="text/css"/>

    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <!-- Toast CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

@section('css')
    @show
</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper box-layout theme-2-active navbar-top-navyblue horizontal-nav">
    <!-- Top Menu Items -->
    @include('System.Layouts.Header')
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    @include('System.Layouts.Menu')
    <!-- /Left Sidebar Menu -->

    <!-- Right Sidebar Menu -->
    @include('System.Layouts.Right-Sidebar')
    <!-- /Right Sidebar Menu -->

    <!-- Main Content -->
    <div class="page-wrapper">
       @section('content')
       @show

        <!-- Footer -->
        @include('System.Layouts.Footer')
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Data table JavaScript -->
<script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="dist/js/jquery.slimscroll.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="../vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="../vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="dist/js/dropdown-bootstrap-extended.js"></script>

<!-- Sparkline JavaScript -->
<script src="../vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="../vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- Switchery JavaScript -->
<script src="../vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- EChartJS JavaScript -->
<script src="../vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
<script src="../vendors/echarts-liquidfill.min.js"></script>

<!-- Vector Maps JavaScript -->
<script src="../vendors/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="../vendors/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="dist/js/vectormap-data.js"></script>

<!-- Toast JavaScript -->
<script src="../vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Piety JavaScript -->
<script src="../vendors/bower_components/peity/jquery.peity.min.js"></script>
<script src="dist/js/peity-data.js"></script>

<!-- Morris Charts JavaScript -->
<script src="../vendors/bower_components/raphael/raphael.min.js"></script>
<script src="../vendors/bower_components/morris.js/morris.min.js"></script>
<script src="../vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Init JavaScript -->
<script src="dist/js/init.js"></script>

<script src="../vendors/bower_components/Flot/excanvas.min.js"></script>
<script src="../vendors/bower_components/Flot/jquery.flot.js"></script>
<script src="../vendors/bower_components/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/bower_components/Flot/jquery.flot.resize.js"></script>
<script src="../vendors/bower_components/Flot/jquery.flot.time.js"></script>
<script src="../vendors/bower_components/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/bower_components/Flot/jquery.flot.crosshair.js"></script>
<script src="../vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="dist/js/flot-data.js"></script>

<!-- Select2 JavaScript -->
<script src="../vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="../vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

{{--<script src="dist/js/dashboard-data.js"></script>--}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    @if(Session::get('flash_level') == 'success')
        toastr.success('{{ Session::get('flash_message') }}', 'Success!', {timeOut: 3500})
    @elseif(Session::get('flash_level') == 'error')
        toastr.error('{{ Session::get('flash_message') }}', 'Error!', {timeOut: 3500})
    @endif

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            toastr.error('{{$error}}', 'Error!', {timeOut: 3500})
        @endforeach
    @endif
</script>

@section('script')
@show
</body>

</html>
