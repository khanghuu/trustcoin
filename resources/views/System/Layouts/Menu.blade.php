<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>Main</span>
            <hr/>
        </li>
        <li>
            <a class="active" href="{{route('Dashboard')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left pull-left-nav"><img src="dist/img/ic-nav/dashboard.png" width="30"><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="active" href="{{route('System.myWallet')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left pull-left-nav"><img src="dist/img/ic-nav/wallet.png" width="30"><span class="right-nav-text">My wallet</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="active" href="{{route('System.Investment')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left pull-left-nav"><img src="dist/img/ic-nav/money.png" width="30"><span class="right-nav-text">Investment</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left pull-left-nav"><img src="dist/img/ic-nav/value.png" width="30"><span class="right-nav-text">Member</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="ecom_dr" class="collapse collapse-level-1">

                <li>
                    <a href="{{route('System.getMembersTree')}}">member-tree</a>
                </li>
                <li>
                    <a href="{{route('System.getMembersList')}}">Member-list</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left pull-left-nav"><img src="dist/img/ic-nav/clock.png" width="30"><span class="right-nav-text">History</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="ecom_dr" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('System.History.getHistoryWallet')}}">History Wallet</a>
                </li>
                <li>
                    <a href="{{route('System.History.getHistoryInvestment')}}">History Commission</a>
                </li>
                <li>
                    <a href="{{route('System.History.getHistoryInvestment')}}">History Investment</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="active" href="ticket.html" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left pull-left-nav"><img src="dist/img/ic-nav/tag.png" width="30"><span class="right-nav-text">Ticket</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
        </li>
        <li class="navigation-header mt-20">
            <span>Admin</span>
            <hr/>
        </li>
        @if(session('user')->User_Level == 1)
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left pull-left-nav"><img src="dist/img/ic-nav/admin-with-cogwheels.png" width="30"><span class="right-nav-text text-yellow">Admin</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
                <ul id="ecom_dr" class="collapse collapse-level-1">
                    <li>
                        <a href="{{route('System.Admin.InvestmentList')}}">Investment</a>
                    </li>
                    <li>
                        <a href="{{route('System.Admin.WalletList')}}">Wallet</a>
                    </li>
                    <li>
                        <a href="{{route('System.Admin.UsersList')}}">User</a>
                    </li>
                    <li>
                        <a href="{{route('System.Admin.MemberList')}}">Member</a>
                    </li>
                    <li>
                        <a href="admin-ticket.html">Ticket</a>
                    </li>
                </ul>
            </li>
        @endif
    </ul>
</div>
