@extends('System.Layouts.Master')
@section('title', 'Members List')
@section('css')
@endsection
@section('content')
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-light">Member list</h6>
                    </div>
                    <div class="pull-right">
                        <h6 class="panel-title txt-light">Total sales: <span class="font-28 text-yellow">$ 0.00</span></h6>
                    </div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="">
                                <table id="myTable1" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Email</th>
                                            <th>Registration Time</th>
                                            <th>Sponsor</th>
                                            <th>Level</th>
                                        </tr>
                                    </thead>
                                    <tbody id="member-table">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>
@endsection
@section('script')
    <script>
        membersList = @json($membersList);
        var html = "";
        showMembersList(membersList)
        $('#member-table').append(html);
        function showMembersList(membersList) {
            $.each(membersList,function(i,member){
                html += "<tr><td>" + member.User_ID + "</td><td>" + member.User_Email + "</td><td>" + member.User_RegisteredDatetime + "</td><td>" + member.User_Parent + "</td><td>F" + member.User_F + "</td></tr>";
                if (member.children) {
                    showMembersList(member.children);
                }
            });
        }
    </script>
@endsection
