@extends('System.Layouts.Master')
@section('title', 'Members Tree')
@section('css')
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <style>
        #tree {
            width: 100%;
            height: 100%;
            position: relative;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid pt-30">
        <div class="row">
            <div id="tree"/>
        </div>
        <!-- /Row -->
    </div>
@endsection
@section('script')
    <script src="https://balkangraph.com/js/latest/OrgChart.js"></script>
    <script>
        $(document).ready(function () {

            var chart = new OrgChart(document.getElementById("tree"), {
                template: "diva",
                enableSearch: false,
                mouseScrool: OrgChart.action.none,
                nodeBinding: {
                    field_0: "email",
                    field_1: "title",
                    img_0: "img"
                },
                nodes: @json($usersTreeList)


            });
        });









    </script>
@endsection
