@extends('System.Layouts.Master')
@section('title', 'Dashboard')
@section('css')
@endsection
@section('content')
<div class="container-fluid pt-50">
    <!-- Row -->
    <!-- Row -->
    <div class="row">
        <div class="col-md-8">

            <div class="row card-view">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label mb-10 fz-18-tf" for="example-input1-group2">Bitcoin</label>
                                <div class="input-group mb-15"> <span class="input-group-btn">
													<button type="button" class="btn btn-bd"><img
                                                            src="dist/img/ic-bit.png" height="27"></button>
													</span>
                                    <input type="text" id="example-input1-group4 bd-rd" name="example-input1-group4"
                                           class="form-control" placeholder="Amount...">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label mb-10 fz-18-tf" for="example-input1-group2">Ethereum</label>
                                <div class="input-group mb-15"> <span class="input-group-btn">
													<button type="button" class="btn btn-bd"><img
                                                            src="dist/img/ic-eth.png" height="27"></button>
													</span>
                                    <input type="text" id="example-input1-group4 bd-rd" name="example-input1-group4"
                                           class="form-control" placeholder="Amount...">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label mb-10 fz-18-tf"
                                       for="example-input1-group2">trustcoin</label>
                                <div class="input-group mb-15"> <span class="input-group-btn">
													<button type="button" class="btn btn-bd"><img
                                                            src="dist/img/ic-trust-5.png" height="27"></button>
													</span>
                                    <input type="text" id="example-input1-group4 bd-rd" name="example-input1-group4"
                                           class="form-control" placeholder="Amount...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="hr-color"></div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group mgg-t-20">
                                <div class="col-sm-12">
                                    <div class="input-group mb-15">
                                        <div class="input-group-btn">
                                            <button type="button"
                                                    class="btn btn-select bd-button btn-info dropdown-toggle"
                                                    data-toggle="dropdown">SELECTION OF PLAN <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)">USD</a></li>
                                                <li><a href="javascript:void(0)">ETH</a></li>
                                            </ul>
                                        </div>
                                        <input type="text" id="example-input1-group3" name="example-input1-group3"
                                               class="form-control" placeholder="Amount...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label mb-10 fz-18-tf">INVESTMENT TERM</label>
                                <select class="selectpicker bd-white "
                                        data-style="form-control btn-default btn-outline">
                                    <option>3 month / 8%</option>
                                    <option>6 month / 10%</option>
                                    <option>9 month / 12%</option>
                                    <option>12 month / 15%</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label mb-10 fz-18-tf" for="example-input1-group2">INTEREST</label>
                                <div class="input-group mb-15">
                                    <div class="input-group-btn">
                                        <select class="selectpicker bd-white"
                                                data-style="form-control btn-default btn-outline">
                                            <option>DAY</option>
                                            <option>MOUNTH</option>
                                            <option>CYCLE</option>
                                        </select>
                                    </div>
                                    <input type="text" id="example-input1-group3" name="example-input1-group3"
                                           class="form-control" placeholder="$0">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label mb-10 fz-18-tf"
                                       for="example-input1-group2">TRUSTCOIN</label>
                                <div class="input-group mb-15"> <span class="input-group-btn">
													<button class="btn  btn-light btn-outline btn-rounded bd-white">Trustcoin</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row ">
                <div class="col-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-light"><i class="fa fa-line-chart" aria-hidden="true"></i>
                                    chart</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="flot-container" style="height:300px">
                                    <div id="flot_line_chart_moving" class="demo-placeholder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light">DataTable</h6>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="">
                                    <table id="myTable1" class="table table-hover display">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>2011/04/25</td>
                                            <td>$320,800</td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                            <td>63</td>
                                            <td>2011/07/25</td>
                                            <td>$170,750</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">
                                                <span class="txt-light block counter bd-tf"><p>MY WALLET</p></span>
                                                <span class="capitalize-font counter block tx-color">$ <span
                                                        class="counter-anim">999</span></span>
                                                <span class="capitalize-font pl-10 block">Withdraw Funds</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <div class="bg-color"></div>
                                                <p class="bd-img-1"><img class="pd-t-20" src="dist/img/ic-trust-3.png"
                                                                         height="70"></p>
                                            </div>
                                        </div>
                                        <div class="progress-anim">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info
																wow animated progress-animated" role="progressbar"
                                                     aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">
                                                <span class="txt-light block counter bd-tf"><p>MY INTEREST</p></span>
                                                <span class="capitalize-font counter block tx-color">$ <span
                                                        class="counter-anim">999</span></span>
                                                <span
                                                    class="capitalize-font pl-10 block">Received amount: <span>$ 0.00</span> <span
                                                        class="text-danger text-semibold"><i class="fa fa-level-down"
                                                                                             aria-hidden="true"></i></span></span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <div class="bg-color"></div>
                                                <p class="bd-img-1"><img class="pd-t-20" src="dist/img/ic-trust-2.png"
                                                                         height="70"></p>
                                            </div>
                                        </div>
                                        <div class="progress-anim">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info
																wow animated progress-animated" role="progressbar"
                                                     aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">
                                                <span class="txt-light block counter bd-tf"><p>MY BONUS</p></span>
                                                <span class="capitalize-font counter block tx-color">$ <span
                                                        class="counter-anim">999</span></span>
                                                <span
                                                    class="capitalize-font pl-10 block">Received amount: <span>$ 0.00</span> <span
                                                        class="text-success text-semibold"><i class="fa fa-level-up"
                                                                                              aria-hidden="true"></i></span></span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <div class="bg-color"></div>
                                                <p class="bd-img-1"><img class="pd-t-20" src="dist/img/ic-trust-1.png"
                                                                         height="70"></p>
                                            </div>
                                        </div>
                                        <div class="progress-anim">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info
																wow animated progress-animated" role="progressbar"
                                                     aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">
                                                <span class="txt-light block counter bd-tf"><p>INVESTMENT</p></span>
                                                <span class="capitalize-font pl-10 counter block tx-color">$ <span
                                                        class="counter-anim">999</span></span>
                                                <span class="capitalize-font pl-10 block">Cycle:</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <div class="bg-color"></div>
                                                <p class="bd-img-1"><img class="pd-t-20" src="dist/img/ic-trust-4.png"
                                                                         height="70"></p>
                                            </div>
                                        </div>
                                        <div class="progress-anim">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info
																wow animated progress-animated" role="progressbar"
                                                     aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">
                                                <span class="txt-light block counter bd-tf"><p>TRUST COIN</p></span>
                                                <span class="capitalize-font counter block tx-color">$ <span
                                                        class="counter-anim">999</span></span>
                                                <span class="capitalize-font pl-10 block">$ 0.00</span>
                                                <span class="capitalize-font pl-10 block">Withdraw Funds</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <div class="bg-color"></div>
                                                <p class="bd-img-1"><img class="pd-t-20" src="../img/LOGO/logo2-01.png"
                                                                         height="70"></p>
                                            </div>
                                        </div>
                                        <div class="progress-anim">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info
																wow animated progress-animated" role="progressbar"
                                                     aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">
                                                <span class="txt-light block counter bd-tf"><p>TRUST WALLET</p></span>
                                                <span class="capitalize-font counter block tx-color">$ <span
                                                        class="counter-anim">999</span></span>
                                                <span class="capitalize-font pl-10  block">$ 0.00</span>
                                                <span class="capitalize-font pl-10 block">Withdraw Funds</span>
                                                <!-- <button class="btn ml-10 btn-success btn-anim"><i class="fa fa-usd"></i><span class="btn-text fa fa-exchange"> Exchange</span></button> -->
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right btn-exchange">
                                                <div class="bg-color"></div>
                                                <p class="bd-img-1"><img class="pd-t-20" src="../img/LOGO/logo2-01.png"
                                                                         height="70"></p>

                                            </div>
                                        </div>
                                        <div class="progress-anim">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info
																wow animated progress-animated" role="progressbar"
                                                     aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>
@endsection
