@extends('System.Layouts.Master')
@section('title', 'Admin-Investment')
@section('css')
@endsection
@section('content')
    <div class="container-fluid">
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light"><i class="fa fa-table" aria-hidden="true"></i> List User</h6>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="myTable1" class="table table-hover display" >
                                        <thead>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="ID">
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="Email">
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="level">
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <div class='input-group date' id='datetimepicker5'>
                                                        <input type='text' class="form-control" />
                                                        <span class="input-group-addon">
																			<span class="fa fa-calendar"></span>
																		</span>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="User Prarent">
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="Agency Level">
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <select class="form-control" tabindex="1">
                                                        <option value="Category 1">Success</option>
                                                        <option value="Category 2">Processing</option>
                                                        <option value="Category 3">Cancel</option>
                                                    </select>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success  mr-10"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary  mr-10"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                                                </div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($usersList as $user)
                                                <tr>
                                                    <td>{{$user->User_ID}}</td>
                                                    <td>{{$user->User_Email}}</td>
                                                    <td>{{$user->User_Level_Name}}</td>
                                                    <td>{{$user->User_RegisteredDatetime}}</td>
                                                    <td>{{$user->User_Parent}}</td>
                                                    <td>{{$user->user_agency_level_Name}}</td>
                                                    <td><a href="{{route('System.Admin.AutoLogin', $user->User_ID)}}" class="pull-left btn btn-danger btn-xs mr-15"><i class="fa fa-sign-out" aria-hidden="true"></i> Login</a></td>
                                                </tr>
                                            @endforeach

{{--                                            <td><span class="pull-left btn btn-success btn-xs mr-15">success</span></td>--}}


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection

