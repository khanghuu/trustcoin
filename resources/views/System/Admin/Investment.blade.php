@extends('System.Layouts.Master')
@section('title', 'Admin-Investment')
@section('css')
@endsection
@section('content')
    <div class="container-fluid">
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default card-view">
                    <!-- <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light">SELECTION OF INVESTMENT PLAN</h6>
                        </div>
                    </div> -->
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="form-wrap">
                                <form>
                                    <div class="form-group">
                                        <label class="control-label mb-10" for="exampleInputpwd_1"><i class="fa fa-user" aria-hidden="true"></i> User ID</label>
                                        <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="Enter User ID">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label mb-10" for="exampleInputEmail_1"><i class="fa fa-usd" aria-hidden="true"></i> Amount</label>
                                        <div class="form-group">
                                            <input class="vertical-spin" type="text" data-bts-button-down-class="btn btn-default"   data-bts-button-up-class="btn btn-default" value="" name="vertical-spin">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label mb-10" for="exampleInputuname_1"><i class="fa fa-chevron-down" aria-hidden="true"></i> Currency</label>
                                        <div class="form-group">
                                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1">
                                                <option value="Category 1">USD</option>
                                                <option value="Category 2">BTC</option>
                                                <option value="Category 3">ETH</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group text-center">
                                        <button class="btn btn-bd1 btn-lg1 btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Invest</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="form-wrap">
                                <form>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputpwd_1"><i class="fa fa-user" aria-hidden="true"></i> ID</label>
                                                    <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="Enter ID">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputuname_1"><i class="fa fa-chevron-down" aria-hidden="true"></i> Status</label>
                                                    <div class="form-group">
                                                        <select class="form-control" tabindex="1">
                                                            <option value="Category 1">Defund</option>
                                                            <option value="Category 2">Success</option>
                                                            <option value="Category 3">Cancel</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputpwd_1"><i class="fa fa-users" aria-hidden="true"></i> User ID</label>
                                                    <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="Enter User ID">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left"><i class="fa fa-calendar" aria-hidden="true"></i> From</label>
                                                    <div class='input-group date' id='datetimepicker1'>
                                                        <input type='text' class="form-control" />
                                                        <span class="input-group-addon">
																		<span class="fa fa-calendar"></span>
																	</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputuname_1"><i class="fa fa-chevron-down" aria-hidden="true"></i> Currency</label>
                                                    <div class="form-group">
                                                        <select class="form-control" tabindex="1">
                                                            <option value="Category 1">USD</option>
                                                            <option value="Category 2">ETH</option>
                                                            <option value="Category 3">BTC</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left"><i class="fa fa-calendar" aria-hidden="true"></i> To</label>
                                                    <div class='input-group date' id='datetimepicker5'>
                                                        <input type='text' class="form-control" />
                                                        <span class="input-group-addon">
																		<span class="fa fa-calendar"></span>
																	</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div>
                                                        <input id="check_box_switch" type="checkbox" data-off-text="False" data-on-text="True"  class="bs-switch">
                                                        <label> &nbsp;Insurrance
                                                            <span id="check_box_value" ></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="form-actions mt-10">
                                                        <button type="submit" class="btn btn-lg1 btn-success  mr-10"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</button>
                                                        <button type="button" class="btn btn-lg1 btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light"><i class="fa fa-table" aria-hidden="true"></i> List Investment Table</h6>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="myTable1" class="table table-hover display" >
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>User ID</th>
                                            <th>Email</th>
                                            <th>Amount</th>
                                            <th>Currency</th>
                                            <th>Insurrance</th>
                                            <th>Time</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>User ID</th>
                                            <th>Email</th>
                                            <th>Amount</th>
                                            <th>Currency</th>
                                            <th>Insurrance</th>
                                            <th>Time</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>2011/04/25</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>2011/04/25</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection
