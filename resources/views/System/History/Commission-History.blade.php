@extends('System.Layouts.Master')
@section('title', 'Commission History')
@section('css')
@endsection
@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
{{--            <div class="col-sm-12">--}}
{{--                <div class="col-md-4">--}}
{{--                    <div class="panel panel-default card-view pa-0">--}}
{{--                        <div class="panel-wrapper collapse in">--}}
{{--                            <div class="panel-body pa-0">--}}
{{--                                <div class="sm-data-box">--}}
{{--                                    <div class="container-fluid">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">--}}
{{--                                                <span class="txt-light block counter bd-tf"><p>AFFILIATE</p></span>--}}
{{--                                                <span class="capitalize-font counter block tx-color">$ <span class="counter-anim">999</span></span>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">--}}
{{--                                                <div class="bg-color"></div>--}}
{{--                                                <p class="bd-img-1"><img class="pd-t-20" src="dist/img/affiliate.png" height="70"></p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="progress-anim">--}}
{{--                                            <div class="progress">--}}
{{--                                                <div class="progress-bar progress-bar-info--}}
{{--															wow animated progress-animated" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-md-4">--}}
{{--                    <div class="panel panel-default card-view pa-0">--}}
{{--                        <div class="panel-wrapper collapse in">--}}
{{--                            <div class="panel-body pa-0">--}}
{{--                                <div class="sm-data-box">--}}
{{--                                    <div class="container-fluid">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">--}}
{{--                                                <span class="txt-light block counter bd-tf"><p>DIRECT</p></span>--}}
{{--                                                <span class="capitalize-font counter block tx-color">$ <span class="counter-anim">999</span></span>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">--}}
{{--                                                <div class="bg-color"></div>--}}
{{--                                                <p class="bd-img-1"><img class="pd-t-20" src="dist/img/upload.png" height="70"></p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="progress-anim">--}}
{{--                                            <div class="progress">--}}
{{--                                                <div class="progress-bar progress-bar-info--}}
{{--															wow animated progress-animated" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-md-4">--}}
{{--                    <div class="panel panel-default card-view pa-0">--}}
{{--                        <div class="panel-wrapper collapse in">--}}
{{--                            <div class="panel-body pa-0">--}}
{{--                                <div class="sm-data-box">--}}
{{--                                    <div class="container-fluid">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-xs-6 text-center pl-15 pr-0 data-wrap-left">--}}
{{--                                                <span class="txt-light block counter bd-tf"><p>INDIRECT</p></span>--}}
{{--                                                <span class="capitalize-font counter block tx-color">$ <span class="counter-anim">999</span></span>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">--}}
{{--                                                <div class="bg-color"></div>--}}
{{--                                                <p class="bd-img-1"><img class="pd-t-20" src="dist/img/give-money.png" height="70"></p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="progress-anim">--}}
{{--                                            <div class="progress">--}}
{{--                                                <div class="progress-bar progress-bar-info--}}
{{--															wow animated progress-animated" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light"><i class="fa fa-history" aria-hidden="true"></i> HISTORY COMMISSION</h6>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="myTable1" class="table table-hover display" >
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Commission Amount</th>
                                            <th>Exchange Rate</th>
                                            <th>Time</th>
                                            <th>Comment</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($commissionList as $commission)
                                                <tr>
                                                    <td>{{$commission->Money_ID}}</td>
                                                    <td>{{$commission->Money_USDT}}</td>
                                                    <td>{{$commission->Money_Rate}}</td>
                                                    <td>{{date('Y-m-d h:m:s', $commission->Money_Time)}}</td>
                                                    <td>{{$commission->Money_Comment}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
