@extends('System.Layouts.Master')
@section('title', 'Wallet History')
@section('css')
@endsection
@section('content')
    <div class="container-fluid">

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light"><i class="fa fa-history" aria-hidden="true"></i> HISTORY WALLET</h6>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="myTable1" class="table table-hover display" >
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Deposit Amount</th>
                                            <th>Money Action</th>
                                            <th>Free</th>
                                            <th>Exchange Rate</th>
                                            <th>Time</th>
                                            <th>Commemt</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($walletHistory as $item)
                                                <tr>
                                                    <td>{{$item['Money_ID']}}</td>
                                                    <td>{{$item['Money_USDT']}}</td>
                                                    <td>{{$item['MoneyAction_Name']}}</td>
                                                    <td>{{$item['Money_USDTFee']}}</td>
                                                    <td>{{$item['Money_Rate']}}</td>
                                                    <td>{{date('Y-m-d h:m:s', $item['Money_Time'])}}</td>
                                                    <td>{{$item['Money_Comment']}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
@endsection
