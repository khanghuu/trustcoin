@extends('System.Layouts.Master')
@section('title', 'Investment History')
@section('css')
@endsection
@section('content')
    <div class="container-fluid">

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light"><i class="fa fa-history" aria-hidden="true"></i> INVESTMENT HISTORY</h6>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="myTable1" class="table table-hover display" >
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Investment Amount</th>
                                            <th>Exchange Rate</th>
                                            <th>Investment Time</th>
                                            <th>Investment Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($investmentHistory as $investmentItem)
                                            <tr>
                                                <td>{{$investmentItem->investment_ID}}</td>
                                                <td>{{$investmentItem->investment_Amount}}</td>
                                                <td>{{$investmentItem->investment_Rate}}</td>
                                                <td>{{date('Y-m-d h:m:s', $investmentItem->investment_Time)}}</td>
                                                @if($investmentItem->investment_Status = 1)
                                                    <td>Active</td>
                                                @else
                                                    <td>Cancel</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
@endsection
