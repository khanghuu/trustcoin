@extends('System.Layouts.Master')
@section('title', 'Investment')
@section('css')
@endsection
@section('content')
    <div class="container-fluid">

        <!-- Row -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light">SELECTION OF INVESTMENT PLAN</h6>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <form method="post" action="{{route('System.postInvestment')}}">
                                            @csrf
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Investment term</label>
                                                <div class="form-group">
                                                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1" disabled>
                                                        <option value="Category 1">3 months</option>
                                                        <option value="Category 2">6 months</option>
                                                        <option value="Category 3">9 months</option>
                                                        <option value="Category 4">12 months</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputEmail_1">Amount</label>
                                                <div class="form-group">
                                                    <input type="text" name="investment_amount" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputpwd_1">Term</label>
                                                <input type="name" class="form-control" id="exampleInputpwd_1" placeholder="Enter Term" disabled>
                                            </div>
                                            <div class="form-group text-center">
                                                <button class="btn btn-bd1 btn-lg1 btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Invest</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light">PLAN FOR 3 MONTHS <span class="font-28 text-yellow">8%</span> MONTHLY INTEREST</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                        <tr>
                                            <th>Investment Term</th>
                                            <th>90 days</th>
                                        </tr>
                                        <tr>
                                            <th>amount available</th>
                                            <th>{{$coinAmount}}</th>
                                        </tr>
                                        <tr>
                                            <th>Investment Coins</th>
                                            <th>{{$investmentAmount}}</th>
                                        </tr>
                                        <tr>
                                            <th>Daily interest</th>
                                            <th>{{$dailyInterest}}</th>
                                        </tr>
                                        <tr>
                                            <th>Total profit</th>
                                            <th>{{$totalProfit}}</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light">INVESTMENT HISTORY</h6>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="myTable1" class="table table-hover display" >
                                        <thead>

                                        <tr>
                                            <th>Investment Amount</th>
                                            <th>Investment Rate</th>
                                            <th>Investment Time</th>
                                            <th>Investment Status</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        @foreach($investmentList as $investment)
                                            <tr>
                                                <td>{{$investment->investment_Amount}}</td>
                                                <td>{{$investment->investment_Rate}}</td>
                                                <td>{{date('Y-m-d h:m:s', $investment->investment_Time)}}</td>
                                                @if($investment->investment_Status = 0)
                                                    <td>Cancel</td>
                                                @elseif($investment->investment_Status = 1)
                                                    <td>Active</td>
                                                @else
                                                    <td>Done</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- Piety JavaScript -->
    <script src="../vendors/bower_components/peity/jquery.peity.min.js"></script>
    <script src="dist/js/peity-data.js"></script>
@endsection
