<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Http\Middleware\LoginMiddleware;

Route::get('test', 'Cron\CronController@getDeposit');

Route::group(['middleware' => 'login.register', 'prefix' => 'login-register'], function (){
    Route::get('/', 'Auth\LoginController@getLoginRegister')->name('getLoginRegister');
    Route::post('/register', 'Auth\RegisterController@postRegister')->name('postRegister');
    Route::post('/login', 'Auth\LoginController@postLogin')->name('postLogin');
    // Route::get('active', 'System\UserController@getActive')->name('getActiveMail');

    Route::get('forgot-password', 'Auth\ForgotPasswordController@getForgotPassword')->name('getForgotPassword');
    Route::post('forgot-password', 'Auth\ForgotPasswordController@postForgotPassword')->name('postForgotPassword');
});

Route::group(['prefix' => 'system', 'middleware' => 'login'], function () {
    Route::get('/', 'System\DashboardController@index')->name('Dashboard');
    Route::get('/logout', 'Auth\LoginController@getLogout')->name('getLogout');

    //Members
    Route::get('/get-members-list', 'System\MemberController@getMembersList')->name('System.getMembersList');
    Route::get('/get-members-tree', 'System\MemberController@getMembersTree')->name('System.getMembersTree');

    //Wallet
    Route::get('my-wallet', 'System\WalletController@index')->name('System.myWallet');
    Route::get('ajax-get-wallet-address', 'System\WalletController@getWalletAddress')->name('System.getWalletAddress');
    Route::post('withdraw', 'System\WalletController@postWithdraw')->name('System.postWithdraw');

    //Investment
    Route::get('investment', 'System\InvestmentController@index')->name('System.Investment');
    Route::post('investment', 'System\InvestmentController@postInvestment')->name('System.postInvestment');

    //History
    Route::group(['prefix' => 'history'], function () {
        Route::get('wallet', 'System\WalletController@getHistoryWallet')->name('System.History.getHistoryWallet');
        Route::get('commission', 'System\CommissionController@getHistoryCommisson')->name('System.History.getHistoryCommisson');
        Route::get('investment', 'System\InvestmentController@getHistoryInvestment')->name('System.History.getHistoryInvestment');
    });

    //Admin section
    Route::group(['middleware'=>'check.permission','prefix'=>'admin'], function (){
        Route::get('users', 'System\AdminController@getAdminUsersList')->name('System.Admin.UsersList');
        Route::get('investment', 'System\AdminController@getAdminInvestmentList')->name('System.Admin.InvestmentList');
        Route::get('wallet', 'System\AdminController@getAdminWalletList')->name('System.Admin.WalletList');
        Route::get('member', 'System\AdminController@getAdminMemberList')->name('System.Admin.MemberList');
        Route::get('auto-login/{id}', 'System\AdminController@autoLogin')->name('System.Admin.AutoLogin');
    });
    Route::group(['prefix'=>'cron'], function () {
        Route::get('deposit', 'Cron\CronController@getDeposit')->name('cron.getDeposit');
    });

});
